import Link from 'next/link';

const Home = (): JSX.Element => {
  return (
    <>
      <ul>
        <li>
          <Link href="/host">
            <a>Host Chat</a>
          </Link>
        </li>
        <li>
          <Link href="/participant">
            <a>Join Chat</a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Home;
