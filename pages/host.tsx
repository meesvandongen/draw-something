import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Head from 'next/head';
import { useRouter } from 'next/router';
import ChatRoom from '../src/Chat/ChatRoom';
import MatchCreator from '../src/MatchCreator';
import Sidebar from '../src/Sidebar';
import HostSideBarPart from '../src/HostSideBarPart/HostSideBarPart';
import ParticipantSideBarPart from '../src/ParticipantSideBarPart';
import ConnectedSketchBook from '../src/ConnectedSketchBook';

const Host = (): JSX.Element => {
  const gameCreated = useSelector(state => state.host.gameCreated);
  const router = useRouter();

  useEffect(() => {
    if (gameCreated) {
      router.push('/game');
    }
  }, [gameCreated, router]);
  return (
    <>
      <Head>
        <title>Create a Game</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <MatchCreator />
    </>
  );
};

export default Host;
