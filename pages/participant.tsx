import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import ChatRoom from '../src/Chat/ChatRoom';
import Sidebar from '../src/Sidebar';
import ParticipantSideBarPart from '../src/ParticipantSideBarPart';
import ConnectForm from '../src/ConnectForm';
import ConnectedSketchBook from '../src/ConnectedSketchBook';

const Participant = (): JSX.Element => {
  const gameCreated = useSelector(state => state.host.gameCreated);
  const router = useRouter();

  useEffect(() => {
    if (gameCreated) {
      router.push('/game');
    }
  });
  return <ConnectForm />;
};

export default Participant;
