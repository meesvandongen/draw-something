import React from 'react';
import { useSelector } from 'react-redux';
import Head from 'next/head';
import ChatRoom from '../src/Chat/ChatRoom';
import MatchCreator from '../src/MatchCreator';
import Sidebar from '../src/Sidebar';
import HostSideBarPart from '../src/HostSideBarPart/HostSideBarPart';
import ParticipantSideBarPart from '../src/ParticipantSideBarPart';
import ConnectedSketchBook from '../src/ConnectedSketchBook';

const Game = () => {
  const isHosting = true;
  return (
    <div>
      <Head>
        <title>Sketch Game</title>
      </Head>
      <Sidebar>
        <ChatRoom />
        {isHosting && <HostSideBarPart />}
        <ParticipantSideBarPart />
      </Sidebar>
      <ConnectedSketchBook />
      <style jsx>
        {`
          display: flex;
          height: 100vh;
        `}
      </style>
    </div>
  );
};

export default Game;
