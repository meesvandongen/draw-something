/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import Head from 'next/head';
import store from '../src/redux/store';

class MyApp extends App {
  render(): JSX.Element {
    const { Component, pageProps } = this.props;
    return (
      <Provider store={store}>
        <Head>
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap"
            rel="stylesheet"
          />
        </Head>
        <Component {...pageProps} />
        <style jsx global>
          {`
            html,
            body {
              margin: 0;
              padding: 0;
              height: 100%;
              width: 100%;
              overflow: hidden;
              font-size: 16px;
              background: #ccc;

              display: flex;
              justify-content: center;
              align-items: center;
            }
            * {
              font-family: 'Roboto', sans-serif;
              box-sizing: border-box;
            }
          `}
        </style>
        <style jsx global>
          {`
            .tippy-tooltip[data-animation='fade'][data-state='hidden'] {
              opacity: 0;
            }
            .tippy-iOS {
              cursor: pointer !important;
              -webkit-tap-highlight-color: transparent;
            }
            .tippy-popper {
              pointer-events: none;
              max-width: calc(100vw - 10px);
              transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
              transition-property: transform;
            }
            .tippy-tooltip {
              position: relative;
              color: #fff;
              border-radius: 4px;
              font-size: 14px;
              line-height: 1.4;
              background-color: #333;
              transition-property: visibility, opacity, transform;
              outline: 0;
            }
            .tippy-tooltip[data-placement^='top'] > .tippy-arrow {
              border-width: 8px 8px 0;
              border-top-color: #333;
              margin: 0 3px;
              transform-origin: 50% 0;
              bottom: -7px;
            }
            .tippy-tooltip[data-placement^='bottom'] > .tippy-arrow {
              border-width: 0 8px 8px;
              border-bottom-color: #333;
              margin: 0 3px;
              transform-origin: 50% 7px;
              top: -7px;
            }
            .tippy-tooltip[data-placement^='left'] > .tippy-arrow {
              border-width: 8px 0 8px 8px;
              border-left-color: #333;
              margin: 3px 0;
              transform-origin: 0 50%;
              right: -7px;
            }
            .tippy-tooltip[data-placement^='right'] > .tippy-arrow {
              border-width: 8px 8px 8px 0;
              border-right-color: #333;
              margin: 3px 0;
              transform-origin: 7px 50%;
              left: -7px;
            }
            .tippy-tooltip[data-interactive][data-state='visible'] {
              pointer-events: auto;
            }
            .tippy-tooltip[data-inertia][data-state='visible'] {
              transition-timing-function: cubic-bezier(0.54, 1.5, 0.38, 1.11);
            }
            .tippy-arrow {
              position: absolute;
              border-color: transparent;
              border-style: solid;
            }
            .tippy-content {
              padding: 5px 9px;
            }
          `}
        </style>
      </Provider>
    );
  }
}

export default MyApp;
