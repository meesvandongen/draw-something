import { useState } from 'react';
import { LineInfo } from './types';
import SketchBook from './Sketchbook';

const ControlledSketchBook = () => {
  const [value, setDrawing] = useState<LineInfo[]>([]);

  return (
    <SketchBook
      value={value}
      onLineAdded={(addedLine): void => {
        setDrawing(currentDrawing => [...currentDrawing, addedLine]);
      }}
    />
  );
};
export default ControlledSketchBook;
