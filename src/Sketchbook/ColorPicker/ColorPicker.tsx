import { useState } from 'react';
import Tippy from '@tippy.js/react';

const colors = [
  '#ff0000',
  '#00ff00',
  '#0000ff',
  '#ffff00',
  '#00ffff',
  '#ff00ff',
  '#ffffff',
  '#000000',
];

const Color = ({
  color,
  onClick,
  selected,
}: {
  color: string;
  onClick: () => void;
  selected: boolean;
}) => {
  return (
    <div
      tabIndex={0}
      aria-checked={selected}
      onClick={() => {
        onClick();
      }}
      onKeyDown={e => {
        if (e.key === 'Enter') {
          onClick();
        }
      }}
      role="radio"
    >
      {selected && '✔️'}
      <style jsx>
        {`
          background-color: ${color};
          width: 40px;
          height: 40px;
          border-radius: 50%;
          margin: 10px;
          box-shadow: inset 0px 0 2px #0000007a;
          text-align: center;
          line-height: 40px;
        `}
      </style>
    </div>
  );
};

export const ColorPicker = ({
  value,
  onChange,
}: {
  value: string;
  onChange: (value: string) => void;
}) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <Tippy
      content={
        <div>
          {colors.map(color => (
            <Color
              color={color}
              onClick={() => {
                onChange(color);
                setExpanded(false);
              }}
              selected={value === color}
            />
          ))}
          <style jsx>
            {`
              width: 100%;
              display: flex;
              flex-wrap: wrap;
            `}
          </style>
        </div>
      }
      placement="left"
      trigger="click"
      theme="light"
      interactive
      arrow={false}
    >
      <div>
        <Color
          color={value}
          onClick={() => {
            setExpanded(true);
          }}
          selected={false}
        />
      </div>
    </Tippy>
  );
};
