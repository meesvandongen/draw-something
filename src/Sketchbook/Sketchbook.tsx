import {
  useRef,
  useEffect,
  useState,
  useLayoutEffect,
  useCallback,
} from 'react';
import Autosizer from 'react-virtualized-auto-sizer';
import useComponentSize from '@rehooks/component-size';
import { configureStyle, getNewCoords, isValidDistance } from './utils';
import { drawPoint, drawCurve, drawLine } from './drawing';
import { LineInfo, CanvasCoordinates } from './types';
import ConfigMenu from './ConfigMenu';
import { ColorPicker } from './ColorPicker/ColorPicker';

const SketchBook = ({
  onLineAdded,
  value,
  disabled,
}: {
  onLineAdded: (line: LineInfo) => void;
  value: LineInfo[];
  disabled: boolean;
}): JSX.Element => {
  const ref = useRef<HTMLCanvasElement>();
  const parentRef = useRef();
  const { width, height } = useComponentSize(parentRef);
  const validDistance = useRef(20);
  const [lineSize, setLineSize] = useState(7);
  const [lineColor, setLineColor] = useState('#333333');
  const [tension, setTension] = useState(1);

  const redrawCanvas = useCallback(
    (changedDrawing: LineInfo[]) => {
      const canvasElement = ref.current;
      const canvasContext = canvasElement.getContext('2d');
      canvasContext.clearRect(0, 0, canvasElement.width, canvasElement.height);

      for (let index = 0; index < changedDrawing.length; index += 1) {
        const element = changedDrawing[index];
        configureStyle(element.style, canvasContext);
        if (element.line.length > 1) {
          drawCurve(element.line, canvasContext, tension);
        } else {
          drawPoint(element.line, canvasContext);
        }
      }
    },
    [tension],
  );

  useEffect(() => {
    const canvasElement = ref.current;
    const canvasContext = canvasElement.getContext('2d');
    configureStyle(
      {
        size: lineSize,
        color: lineColor,
      },
      canvasContext,
    );
  }, [lineColor, lineSize]);

  useLayoutEffect(() => {
    const canvasElement = ref.current;
    const canvasContext = canvasElement.getContext('2d');
    canvasContext.canvas.width = width;
    canvasContext.canvas.height = height;
    redrawCanvas(value);
  }, [height, redrawCanvas, value, width]);

  useLayoutEffect(() => {
    const canvasElement = ref.current;
    if (canvasElement) {
      redrawCanvas(value);
    }
  }, [value, redrawCanvas]);

  useEffect(() => {
    const canvasElement = ref.current;
    let currentLine: CanvasCoordinates[] = [];
    let lastUserCoords: CanvasCoordinates = null;

    const onDrawingComplete = (): void => {
      onLineAdded({
        style: {
          size: lineSize,
          color: lineColor,
        },
        line: [...currentLine],
      });
      currentLine = [];
    };

    const addNewLineCoords = (newCoords: CanvasCoordinates): void => {
      currentLine.push({
        x: newCoords.x,
        y: newCoords.y,
      });
    };

    const onMouseMove = (event: MouseEvent): void => {
      event.preventDefault();
      const canvasContext = canvasElement.getContext('2d');
      const newCoords = getNewCoords(event, canvasElement);
      drawLine(lastUserCoords, newCoords, canvasContext);
      lastUserCoords = newCoords;
      if (
        isValidDistance(
          validDistance.current,
          currentLine[currentLine.length - 1],
          newCoords,
        )
      ) {
        addNewLineCoords(newCoords);
      }
    };

    const onMouseDown = (event: MouseEvent): void => {
      event.preventDefault();
      canvasElement.removeEventListener('mousedown', onMouseDown);
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      canvasElement.addEventListener('mouseup', onMouseUp);
      canvasElement.addEventListener('mousemove', onMouseMove);
      const newCoords = getNewCoords(event, canvasElement);
      addNewLineCoords(newCoords);
      lastUserCoords = newCoords;
    };
    const onMouseUp = (event: MouseEvent): void => {
      event.preventDefault();
      canvasElement.removeEventListener('mousemove', onMouseMove);
      canvasElement.removeEventListener('mouseup', onMouseUp);
      canvasElement.addEventListener('mousedown', onMouseDown);
      const newCoords = getNewCoords(event, canvasElement);
      lastUserCoords = null;
      if (
        currentLine[currentLine.length - 1].x !== newCoords.x &&
        currentLine[currentLine.length - 1].y !== newCoords.y
      ) {
        addNewLineCoords(newCoords);
      }
      onDrawingComplete();
    };

    if (canvasElement && !disabled) {
      canvasElement.addEventListener('mousedown', onMouseDown);
    }

    return () => {
      if (canvasElement && !disabled) {
        canvasElement.removeEventListener('mousedown', onMouseDown);
      }
    };
  }, [disabled, lineColor, lineSize, onLineAdded, ref]);

  return (
    <>
      <div ref={parentRef}>
        <canvas ref={ref} />
        <style jsx>
          {`
            width: 100%;
          `}
        </style>
      </div>
      <ConfigMenu>
        <div>
          <div>{lineSize}</div>
          <input
            type="range"
            value={lineSize}
            onChange={e => setLineSize(+e.target.value)}
            min={1}
            max={99}
            step={3}
          />
          <style jsx>
            {`
              input {
                writing-mode: bt-lr; /* IE */
                -webkit-appearance: slider-vertical; /* WebKit */
                padding: 0 5px;
                width: 8px;
                height: 175px;
              }
            `}
          </style>
        </div>
        <ColorPicker value={lineColor} onChange={setLineColor} />
      </ConfigMenu>
    </>
  );
};

export default SketchBook;
