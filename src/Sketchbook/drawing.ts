import { CanvasCoordinates } from './types';

export const drawPoint = (
  points: CanvasCoordinates[],
  canvasContext: CanvasRenderingContext2D,
): void => {
  canvasContext.beginPath();
  canvasContext.moveTo(points[0].x, points[0].y);
  canvasContext.lineTo(points[0].x + 1, points[0].y);
  canvasContext.stroke();
};

export const drawLine = (
  points1: CanvasCoordinates,
  points2: CanvasCoordinates,
  canvasContext: CanvasRenderingContext2D,
): void => {
  canvasContext.beginPath();
  canvasContext.moveTo(points1.x, points1.y);
  canvasContext.lineTo(points2.x, points2.y);
  canvasContext.stroke();
};

export const drawCurve = (
  points: CanvasCoordinates[],
  canvasContext: CanvasRenderingContext2D,
  tension: number,
): void => {
  canvasContext.beginPath();
  canvasContext.moveTo(points[0].x, points[0].y);

  const t = tension != null ? tension : 1;
  for (let i = 0; i < points.length - 1; i += 1) {
    const p0 = i > 0 ? points[i - 1] : points[0];
    const p1 = points[i];
    const p2 = points[i + 1];
    const p3 = i !== points.length - 2 ? points[i + 2] : p2;

    const cp1x = p1.x + ((p2.x - p0.x) / 6) * t;
    const cp1y = p1.y + ((p2.y - p0.y) / 6) * t;

    const cp2x = p2.x - ((p3.x - p1.x) / 6) * t;
    const cp2y = p2.y - ((p3.y - p1.y) / 6) * t;

    canvasContext.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, p2.x, p2.y);
  }
  canvasContext.stroke();
};
