import { CanvasCoordinates, LineStyle } from './types';

export const isValidDistance = (
  minDistance: number,
  coords1: CanvasCoordinates,
  coords2: CanvasCoordinates,
): boolean => {
  if (
    (coords1.x - coords2.x) ** 2 + (coords1.y - coords2.y) ** 2 >
    minDistance ** 2
  ) {
    return true;
  }
  return false;
};

export const configureStyle = (
  style: LineStyle,
  canvasContext: CanvasRenderingContext2D,
): void => {
  /* eslint-disable no-param-reassign */
  canvasContext.lineCap = 'round';
  canvasContext.lineWidth = style.size;
  canvasContext.strokeStyle = style.color;
  /* eslint-enable no-param-reassign */
};

export const getNewCoords = (
  event: MouseEvent,
  canvasElement: HTMLCanvasElement,
): CanvasCoordinates => {
  return {
    x: event.clientX - canvasElement.offsetLeft,
    y: event.clientY - canvasElement.offsetTop,
  };
};
