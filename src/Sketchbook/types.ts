export type CanvasCoordinates = {
  x: number;
  y: number;
};
export type LineStyle = {
  size: number;
  color: string;
};

export type LineInfo = {
  line: CanvasCoordinates[];
  style: LineStyle;
};

