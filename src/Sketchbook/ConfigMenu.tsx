const ConfigMenu = ({
  children,
}: {
  children: React.ReactNode;
}): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          position: absolute;
          right: 0;
        `}
      </style>
    </div>
  );
};

export default ConfigMenu;
