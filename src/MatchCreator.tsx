import { useReducer } from 'react';
import { createAction, PayloadAction, createSlice } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { createGame, GameSettings } from './redux/host/sketchActions';

const dictionaries = [
  {
    name: 'animals',
    json: () =>
      fetch('/static/word-lists/animals.en.json').then(response =>
        response.json(),
      ),
  },
  {
    name: 'animals2',
    json: () =>
      fetch('/static/word-lists/animals.en.json').then(response =>
        response.json(),
      ),
  },
];

const initialState: GameSettings = {
  roomName: '',
  language: '',
  dictionary: dictionaries[0],
  roomSize: 2,
  nGames: 20,
  gameTime: 1000 * 60 * 2,
};
export const setRoomName = createAction<string>('setRoomName');
export const setDictionary = createAction<string>('setDictionary');
export const setRoomSize = createAction<number>('setRoomSize');
export const setNGames = createAction<number>('setNGames');
export const setGameTime = createAction<number>('setGameTime');

const reducer = (
  state: GameSettings,
  action: PayloadAction<any>,
): GameSettings => {
  switch (action.type) {
    case setRoomName.toString():
      return { ...state, roomName: action.payload };
    case setDictionary.toString():
      return { ...state, dictionary: dictionaries[action.payload] };
    case setRoomSize.toString():
      return { ...state, roomSize: action.payload };
    case setNGames.toString():
      return { ...state, nGames: action.payload };
    case setGameTime.toString():
      return { ...state, gameTime: action.payload };
    default:
      throw new Error();
  }
};

const MatchCreator = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const dispatchRedux = useDispatch();
  return (
    <>
      <form
        onSubmit={e => {
          e.preventDefault();
          dispatchRedux(createGame(state));
        }}
      >
        <h1>Create Match</h1>
        {/* <label htmlFor="room-name">
        <h2>Room Name</h2>
        <input
          type="text"
          name="room-name"
          id="room-name"
          defaultValue={state.roomName}
          onChange={e => {
            dispatch(setRoomName(e.target.value));
          }}
        />
      </label> */}
        {/* <label htmlFor="language">
        <h2>Language</h2>
        <input type="text" name="language" id="language" />
      </label> */}
        <label htmlFor="dictionary">
          <h2>Dictionary</h2>
          <select
            onChange={e => {
              dispatch(setDictionary(e.target.value));
            }}
          >
            {dictionaries.map((dict, i) => {
              return <option value={i}>{dict.name}</option>;
            })}
          </select>
          {/* <input
          type="text"
          name="dictionary"
          id="dictionary"
          defaultValue={state.dictionary}
          onChange={e => {
            dispatch(setDictionary(e.target.value));
          }}
        /> */}
        </label>
        <label htmlFor="n-games">
          <h2>Number of Games</h2>
          <input
            type="number"
            name="n-games"
            id="n-games"
            defaultValue={state.nGames}
            onChange={e => {
              dispatch(setNGames(+e.target.value));
            }}
          />
        </label>
        <div>
          <input type="submit" value="Create" />
        </div>
      </form>
      <style jsx>
        {`
          form {
            width: 500px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
            padding: 50px;
            border-radius: 50px;
            background: #cccccc;
            box-shadow: 20px 20px 60px #adadad, -20px -20px 60px #ebebeb;
            margin-top: 20px;
          }
          h1 {
            font-size: 1rem;
            text-transform: uppercase;
            margin-top: 0;
            font-weight: 800;
            color: #1f1f1f;
            text-align: center;
          }
          h2 {
            margin-bottom: 0;
            font-size: 1rem;
            color: #1f1f1f;
          }
          input[type='submit'] {
            margin: 20px 0 0 0;
            width: 100%;
            padding: 10px;
            text-transform: uppercase;
            font-weight: 900;

            border: 0;

            border-radius: 7px;
            background: #cccccc;
            box-shadow: inset 0 0 0 #adadad, inset 0 0 0 #ebebeb,
              4px 4px 8px #adadad, -4px -4px 8px #ebebeb;
            transition: box-shadow 100ms;
          }
          input[type='submit']:active {
            background: #cccccc;
            box-shadow: inset 4px 4px 8px #adadad, inset -4px -4px 8px #ebebeb,
              0px 0px 0px #adadad, 0px 0px 0px #ebebeb;
          }
          input[type='submit']:focus {
            outline: none;
          }
          input[type='number'],
          input[type='text'] {
            width: 100%;
            padding: 10px;
            border: 0;

            border-radius: 7px;
            background: #cccccc;
            box-shadow: inset 4px 4px 8px #adadad, inset -4px -4px 8px #ebebeb;
          }

          select {
            padding: 10px;
            border: 0;

            border-radius: 7px;
            background: #cccccc;
            box-shadow: inset 4px 4px 8px #adadad, inset -4px -4px 8px #ebebeb;
          }
        `}
      </style>
    </>
  );
};

export default MatchCreator;
