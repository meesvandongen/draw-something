const Sidebar = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <style jsx>
        {`
          background: #fff;
          box-shadow: 0 0 9px #545454;
          z-index: 1;
        `}
      </style>
      {children}
    </div>
  );
};

export default Sidebar;
