import { useDispatch, useSelector } from 'react-redux';
import SketchBook from './Sketchbook/Sketchbook';
import { hostSlice } from './redux/host/host-redux-slice';
import { roles } from './redux/host/participant-redux-slice';

const ConnectedSketchBook = () => {
  const dispatch = useDispatch();
  const lines = useSelector(state => state.participant.lines);
  const isDrawingPlayer = useSelector(
    state => state.participant.role === roles.drawer,
  );

  return (
    <SketchBook
      value={lines}
      disabled={!isDrawingPlayer}
      onLineAdded={line => {
        dispatch(hostSlice.actions.drawLine({ line }));
      }}
    />
  );
};

export default ConnectedSketchBook;
