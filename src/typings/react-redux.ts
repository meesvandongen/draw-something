import { RootState } from '../redux/store';

declare module 'react-redux' {
  export function useSelector<TState, TSelected>(
    selector: (state: RootState) => TSelected,
    equalityFn?: (left: TSelected, right: TSelected) => boolean,
  ): TSelected;
}
