import { useRef, useCallback } from 'react';

export type Publish = (...data: any[]) => void;
export type Unsubscribe = (subscriptionIndex: number) => () => void;
export type Subscribe = (onPub: Publish) => ReturnType<Unsubscribe>;

const usePubSub = (): [Publish, Subscribe] => {
  const subScribers = useRef(new Map<number, Publish>());
  const index = useRef(0);

  const unsubscribe: Unsubscribe = subscriptionIndex => (): void => {
    console.log('unsub', subscriptionIndex);
    subScribers.current.delete(subscriptionIndex);
  };

  const subscribe: Subscribe = useCallback(onPub => {
    const subscriptionIndex = index.current;
    index.current += 1;
    subScribers.current.set(subscriptionIndex, onPub);
    return unsubscribe(subscriptionIndex);
  }, []);

  const publish: Publish = useCallback((...data) => {
    subScribers.current.forEach((handler, key) => {
      handler(...data);
    });
  }, []);

  return [publish, subscribe];
};

export default usePubSub;
