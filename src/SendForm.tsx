import { useState } from 'react';
import { usePeerSend } from './Peer/PeerProvider';

const SendForm = ({ id }: { id: string }): JSX.Element => {
  const [sendText, setSendText] = useState('');
  const { send, isConnected } = usePeerSend(id);

  return (
    <>
      <input
        type="text"
        value={sendText}
        onChange={e => setSendText(e.target.value)}
      />
      <button
        type="button"
        onClick={(): void => {
          send({
            type: 'message',
            payload: {
              author: id,
              text: sendText,
            },
          });
        }}
        disabled={!isConnected}
      >
        send to {id}
      </button>
    </>
  );
};

export default SendForm;
