import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { connectToPeer } from './redux/host/peerActions';

const ConnectForm = (): JSX.Element => {
  const [connectText, setConnectText] = useState('');
  const dispatch = useDispatch();

  return (
    <>
      <input
        type="text"
        value={connectText}
        onChange={e => setConnectText(e.target.value)}
      />
      <button
        type="button"
        onClick={(): void => {
          dispatch(connectToPeer(connectText));
        }}
      >
        connect
      </button>
    </>
  );
};

export default ConnectForm;
