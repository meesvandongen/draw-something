import { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { chatSlice } from '../redux/chat/chat-redux-slice';

const ChatRoom = (): JSX.Element => {
  const messages = useSelector(state => state.chat.messages);
  const lastMessageRef = useRef<HTMLDivElement>(null);
  const dispatch = useDispatch();
  const [messageText, setMessageText] = useState('');
  useEffect(() => {
    lastMessageRef.current.scrollIntoView({
      behavior: 'smooth',
    });
  }, [messages]);
  return (
    <div>
      <div className="messages">
        {messages.map(message => (
          <div>
            {message.author}: {message.content}
          </div>
        ))}
        <div ref={lastMessageRef} />
      </div>
      <input
        className="sendMessageBox"
        type="text"
        value={messageText}
        onChange={e => {
          setMessageText(e.target.value);
        }}
        onKeyPress={e => {
          if (e.key === 'Enter') {
            dispatch(
              chatSlice.actions.postMessage({
                content: messageText,
                playerId: '',
              }),
            );
            setMessageText('');
          }
        }}
      />
      <style jsx>
        {`
          .messages {
            height: 150px;
            overflow-y: auto;
          }
          .sendMessageBox {
            width: 100%;
            border-width: 1px 0;
            border-style: solid;
            border-color: #333;
            padding: 4px;
            margin: 0;
            display: block;
          }
        `}
      </style>
    </div>
  );
};

export default ChatRoom;
