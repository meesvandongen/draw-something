const RightSideBar = ({ children }) => {
  return (
    <>
      <div className="sidebar">{children}</div>
      <style jsx>
        {`
          .sidebar {
            position: absolute;
            bottom: 0;
            right: 0;
            top: 0;
            width: 300px;
            background: #ccc;
          }
        `}
      </style>
    </>
  );
};

export default RightSideBar;
