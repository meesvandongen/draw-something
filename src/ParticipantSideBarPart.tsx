import { useSelector } from 'react-redux';

const ParticipantSideBarPart = () => {
  const players = useSelector(state => state.participant.participants);
  const drawer = useSelector(state => state.participant.drawerId);
  return (
    <div>
      <span>Players</span>
      {players.map(player => {
        return (
          <div>
            {player.name} {player.playerId === drawer && '(Drawing)'}
          </div>
        );
      })}
    </div>
  );
};

export default ParticipantSideBarPart;
