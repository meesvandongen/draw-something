import {
  useEffect,
  useCallback,
  useState,
  createContext,
  useContext,
  useMemo,
} from 'react';
import PT, { DataConnection } from 'peerjs';
import usePubSub, { Subscribe } from '../usePubSub';

type PeerData = {
  type: string;
  payload: any;
};

type PeerServerHook = {
  connections: Map<string, DataConnection>;
  isConnected: boolean;
  id: string;
  send: (id: string, data: PeerData) => void;
  addPeer: (id: string) => void;
  broadcast: (data: PeerData) => void;
};

const doNothing = (): void => {
  // do nothing
};

const sendFromConnection = (connection: PT.DataConnection, data) => {
  if (connection) {
    if (connection.open) {
      connection.send(data);
    }
  } else {
    console.error('connection not established, cannot send');
  }
};

export const usePeerServer = (
  onPeerData: (peerId: string, data: PeerData) => void = doNothing,
  onError: (error: Error) => void = doNothing,
  onNewConnection: (peerId: string) => void = doNothing,
): PeerServerHook => {
  const [peerId, setPeerId] = useState(null);
  const [peer, setPeer] = useState(null); // own peer object
  const [connections, setConnections] = useState(
    new Map<string, DataConnection>(),
  );

  const addNewConnection = useCallback(
    newConnection => {
      onNewConnection(newConnection.peer);
      newConnection.on('data', data => {
        onPeerData(newConnection.peer, data);
      });
      setConnections(
        previousConnections =>
          new Map(previousConnections.set(newConnection.peer, newConnection)),
      );
    },
    [onNewConnection, onPeerData],
  );

  useEffect(() => {
    const Peer: typeof PT = require('peerjs').default;
    if (!peer) {
      const tempPeer = new Peer(null, {
        debug: 2,
      });

      tempPeer.on('open', (id: string) => {
        if (id !== null) {
          setPeerId(tempPeer.id);
        }
      });
      tempPeer.on('disconnected', () => {
        // Workaround for peer.reconnect deleting previous id
        // tempPeer.id = lastPeerId;
        // tempPeer._lastServerId = lastPeerId;
        tempPeer.reconnect();
      });
      tempPeer.on('connection', addNewConnection);

      tempPeer.on('close', () => {
        console.log('Connection destroyed');
      });
      tempPeer.on('error', err => {
        onError(err);
      });
      setPeer(tempPeer);
    }
  }, [addNewConnection, onError, onPeerData, peer]);

  const send = useCallback(
    (id, data) => {
      const connection = connections.get(id);
      sendFromConnection(connection, data);
    },
    [connections],
  );

  const broadcast = useCallback(
    data => {
      connections.forEach(connection => {
        sendFromConnection(connection, data);
      });
    },
    [connections],
  );

  const addPeer = useCallback(
    newPeerId => {
      const connection = peer.connect(newPeerId);
      addNewConnection(connection);
    },
    [addNewConnection, peer],
  );

  return {
    connections,
    isConnected: !!peerId,
    id: peerId,
    send,
    addPeer,
    broadcast,
  };
};

type Message = {
  peerId: string;
  messageId: number;
  message: PeerData;
};

const PeerContext = createContext<
  PeerServerHook & {
    subscribe: Subscribe;
    publishLocally: any;
  }
>(null);

export const usePeerServerId = (): string => {
  const { id } = useContext(PeerContext);
  return id;
};

export const usePeerConnections = (): Map<string, PT.DataConnection> => {
  const { connections } = useContext(PeerContext);
  return connections;
};

export const usePeerConnect = (): ((id: string) => void) => {
  const { addPeer } = useContext(PeerContext);
  return addPeer;
};

type SendToSpecificPeer = {
  send: (data: PeerData) => void;
  isConnected: boolean;
};
type SendGeneral = (id: string, data: PeerData) => void;
export function usePeerSend(): SendGeneral;
export function usePeerSend(id: string): SendToSpecificPeer;
export function usePeerSend(id?: string): SendGeneral | SendToSpecificPeer {
  const { send, connections } = useContext(PeerContext);
  if (!id) {
    return send;
  }
  return {
    isConnected: !!connections.get(id),
    send: (data: PeerData): void => {
      send(id, data);
    },
  };
}

export const usePeerBroadCast = (): ((message: PeerData) => void) => {
  const { broadcast } = useContext(PeerContext);

  return broadcast;
};

export const usePeerIds: () => string[] = () => {
  const connections = usePeerConnections();
  const peerIds = useMemo(() => {
    return Array.from(connections.keys());
  }, [connections]);
  return peerIds;
};

export const useIsConnected = (id?: string): boolean => {
  const connections = usePeerConnections();

  return id ? !!connections.get(id) : connections.size > 0;
};

export const usePeerSubscription = (
  onPub: (peerId: string, data: PeerData) => void,
): void => {
  const { subscribe } = useContext(PeerContext);
  useEffect(() => {
    const unsubscribe = subscribe(onPub);
    return unsubscribe;
  }, [onPub, subscribe]);
};

export const useLocalPeerPublish = () => {
  const { publishLocally } = useContext(PeerContext);
  return publishLocally;
};

export const PeerProvider = ({
  children,
}: {
  children: React.ReactNode;
}): JSX.Element => {
  const [pub, sub] = usePubSub();

  const onPeerData = useCallback(
    (peerId, data) => {
      pub(peerId, data);
    },
    [pub],
  );

  const onPeerError = useCallback(error => {
    console.error(error);
  }, []);

  const onPeerConnected = useCallback(
    peerId => {
      pub(peerId, {
        type: 'peer-connected',
        payload: peerId,
      });
    },
    [pub],
  );

  const peerData = usePeerServer(onPeerData, onPeerError, onPeerConnected);

  return (
    <PeerContext.Provider
      value={{
        ...peerData,
        subscribe: sub,
        publishLocally: (...data) => {
          pub(peerData.id, ...data);
        },
      }}
    >
      {children}
    </PeerContext.Provider>
  );
};
