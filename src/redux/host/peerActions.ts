import { createAction, PayloadAction } from '@reduxjs/toolkit';

export type PeerId = string;
export type PeerSent = {
  peerId: PeerId;
};

/**
 * Actions peer dispatches
 */
export const peerDisconnected = createAction<string>('peerDisconnected');
export const peerOpened = createAction<string>('peerOpened');
export const peerError = createAction<Error>('peerError');

export const connectionDataSent = createAction<PeerSent & { data: any }>(
  'connectionDataSent',
);
export const connectionEstablished = createAction<string>(
  'connectionEstablished',
);
export const connectionClosed = createAction<string>('connectionClosed');

/**
 * Actions peer listens to
 */
export const connectToPeer = createAction<string>('connectToPeer');
export const sendToPeer = createAction<{
  peerId: string;
  action: PayloadAction<any>;
}>('sendToPeer');
export const sendToAllPeers = createAction<PayloadAction<any>>('sendToAllPeers');

export const remotePeerMessage = createAction<string>('remotePeerMessage');
