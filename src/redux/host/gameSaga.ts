import {
  put,
  call,
  take,
  select,
  all,
  takeLatest,
  takeEvery,
  putResolve,
  delay,
  race,
} from 'redux-saga/effects';
import { shuffle } from 'lodash';
import {
  gameStarted,
  minimumPlayersRequirmentMet,
  minimumPlayersRequirmentUnMet,
  playerLeft,
  //   startHosting,
  playerJoined,
  playerGuess,
  earlyGameEnd,
  //   newWord,
  createGame,
  setDrawingPlayer,
  setPlayerList,
} from './sketchActions';
import { chatSlice } from '../chat/chat-redux-slice';
import {
  peerOpened,
  connectionDataSent,
  peerError,
  sendToAllPeers,
  sendToPeer,
} from './peerActions';
import { hostSlice } from './host-redux-slice';

function* sendToAllPlayers(message) {
  yield put(sendToAllPeers(message));
}

function* sendToDrawingPlayer(message) {
  const peerId = yield select(state => state.host.drawerId);
  yield put(
    sendToPeer({
      peerId,
      action: message,
    }),
  );
}

function* getNewDrawingPlayer() {
  const players = yield select(state => state.host.players);
  const currentDrawingPlayer = yield select(state => state.host.drawer);
  let nextDrawingPlayerIndex =
    players.findIndex(player => player.playerId === currentDrawingPlayer) + 1;
  if (nextDrawingPlayerIndex > players.length - 1) {
    nextDrawingPlayerIndex = 0;
  }
  console.log(players, nextDrawingPlayerIndex);
  yield call(
    sendToAllPlayers,
    setDrawingPlayer(players[nextDrawingPlayerIndex].playerId),
  );
}
function* getNewWord() {
  const wordList = yield select(state => state.host.wordList);
  const currentWord = yield select(state => state.host.word);
  const maxIndex = wordList.length - 1;
  const currentWordIndex = wordList.indexOf(currentWord);
  const newWordIndex =
    currentWordIndex + 1 > maxIndex ? 0 : currentWordIndex + 1;
  yield call(sendToDrawingPlayer, wordList[newWordIndex]);
}

function* getShuffledWordList(getWordList) {
  const wordList = yield getWordList();
  console.log(wordList);
  const newWordList = shuffle(wordList);
  // yield call(sendToAllPlayers, hostSlice.actions.setWordList(newWordList));
  yield put(hostSlice.actions.setWordList(newWordList));
}

function* gameSaga(action) {
  console.log('waiting for more players to join.');
  yield take(minimumPlayersRequirmentMet.toString());
  console.log('all players joined');
  yield call(getShuffledWordList, action.payload.dictionary.json);
  // yield call(getInitialWordList);
  const nGames = yield select(state => state.host.nGames);
  for (let i = 0; i < nGames; i += 1) {
    console.log(`game: ${i}`);
    yield call(sendToAllPlayers, gameStarted());
    yield call(getNewDrawingPlayer);
    yield call(getNewWord);
    const gameTime = yield select(state => state.host.gameTime);

    yield race({
      timeLimit: delay(gameTime),
      earlyEnd: take(earlyGameEnd),
    });
  }
}

function* playerGuessSaga({ payload: { guess, playerId } }) {
  const word = yield select(state => state.host.word);
  const drawer = yield select(state => state.host.drawerId);
  if (guess === word && playerId !== drawer) {
    yield put(earlyGameEnd());
  }
}

function* playerJoinSaga(action) {
  yield put(
    chatSlice.actions.postSystemMessage({
      content: `player "${action.payload.playerId}" joined the game.`,
    }),
  );
  const players = yield select(state => state.host.players);
  yield sendToAllPlayers(
    setPlayerList(
      players.map(player => ({
        id: player.playerId,
        name: player.name,
      })),
    ),
  );
  const minimumPlayers = yield select(state => state.host.minimumPlayers);
  if (players.length >= minimumPlayers) {
    yield put(minimumPlayersRequirmentMet());
  }
}

function* playerLeaveSaga(action) {
  yield put(
    chatSlice.actions.postSystemMessage({
      content: `player "${action.payload.playerId}" left the game.`,
    }),
  );
  const players = yield select(state => state.host.players);
  const minimumPlayers = yield select(state => state.host.minimumPlayers);
  if (players.length < minimumPlayers) {
    yield put(minimumPlayersRequirmentUnMet());
  }
}
export function* hostSaga() {
  yield all([
    takeEvery(playerJoined, playerJoinSaga),
    takeEvery(playerLeft, playerLeaveSaga),
    takeLatest(createGame, gameSaga),
    takeEvery(playerGuess, playerGuessSaga),
  ]);
}
