import {
  put,
  call,
  take,
  select,
  all,
  takeLatest,
  takeEvery,
  putResolve,
  delay,
  race,
} from 'redux-saga/effects';
import {
  connectionEstablished,
  peerDisconnected,
  peerOpened,
  peerError,
  connectionDataSent,
  connectionClosed,
} from './peerActions';
import {
  playerJoined,
  playerDisconnected,
  playerReconnected,
  playerLeft,
} from './sketchActions';

function* peerDisconnectedSaga({ payload: peerId }) {
  yield put(playerDisconnected(peerId));
  const { timeout, reconnect } = yield race({
    timeout: delay(1000 * 3),
    // TODO: implement reconnect ability
    reconnect: take(playerReconnected),
  });
  if (timeout) {
    yield put(playerLeft(peerId));
  }
}
function* peerOpenedSaga({ payload: id }) {
  const peerId = id;
  yield put(
    playerJoined({
      playerId: peerId,
      name: peerId,
      peerId,
    }),
  );
}
function* peerErrorSaga() {}
function* connectionDataSentSaga({ payload: { peerId, data } }) {
  yield console.log(peerId, data);
}
function* connectionEstablishedSaga({ payload: peerId }) {
  yield put(
    playerJoined({
      playerId: peerId,
      name: peerId,
      peerId,
    }),
  );
}
function* connectionClosedSaga() {}

export function* peerSaga() {
  yield all([
    takeEvery(peerDisconnected, peerDisconnectedSaga),
    takeEvery(peerOpened, peerOpenedSaga),
    takeEvery(peerError, peerErrorSaga),
    takeEvery(connectionDataSent, connectionDataSentSaga),
    takeEvery(connectionEstablished, connectionEstablishedSaga),
    takeEvery(connectionClosed, connectionClosedSaga),
  ]);
}
