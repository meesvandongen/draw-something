import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LineInfo } from '../../Sketchbook/types';
import {
  drawLine,
  clearCanvas,
  setDrawingPlayer,
  setPlayerList,
} from './sketchActions';

export enum roles {
  guesser,
  drawer,
}

const participant = createSlice({
  initialState: {
    role: roles.guesser,
    lines: [],
    participants: [],
    drawerId: '',
  },
  name: 'participant',
  reducers: {},
  extraReducers: {
    [drawLine.toString()]: (
      state,
      action: PayloadAction<{
        line: LineInfo;
      }>,
    ) => {
      state.lines.push(action.payload.line);
    },
    [clearCanvas.toString()]: state => {
      state.lines = [];
    },
    [setDrawingPlayer.toString()]: (
      state,
      action: ReturnType<typeof setDrawingPlayer>,
    ) => {
      state.drawerId = action.payload;
    },
    [setPlayerList.toString()]: (
      state,
      action: ReturnType<typeof setPlayerList>,
    ) => {
      state.participants = action.payload;
    },
  },
});

export default participant;
