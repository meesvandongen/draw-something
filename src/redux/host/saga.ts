import { fork } from 'redux-saga/effects';
import { peerSaga } from './peerSaga';
import { hostSaga } from './gameSaga';

export function* rootSaga() {
  yield fork(peerSaga);
  yield fork(hostSaga);
}
