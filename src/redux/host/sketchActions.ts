import { createAction, PayloadAction } from '@reduxjs/toolkit';
import { LineInfo } from '../../Sketchbook/types';
import { PeerSent, PeerId } from './peerActions';

export type PlayerId = string;
export type PlayerSent = {
  playerId: PlayerId;
};

export const draw = createAction<LineInfo>('draw');

export const requestDrawing = createAction('requestDrawing');
export const playerGuess = createAction<
  {
    guess: string;
  } & PlayerSent
>('playerGuess');

export const setPlayerList = createAction<
  {
    id: PlayerId;
    name: string;
  }[]
>('setPlayerList');

export const clearCanvas = createAction('clearCanvas');
export const drawLine = createAction('drawLine');
export const setDrawingPlayer = createAction<string>('setDrawingPlayer');

export const startHosting = createAction('startHosting');
type PlayerJoined = PlayerSent & PeerSent & { name: string };
export const playerJoined = createAction<PlayerJoined>('playerJoined');
export const playerDisconnected = createAction<PeerId>('playerDisconnected');
export const playerReconnected = createAction<PlayerId>('playerReconnected');
export const playerLeft = createAction<PeerId>('playerLeft');
export const earlyGameEnd = createAction<PeerId>('earlyGameEnd');

export const minimumPlayersRequirmentMet = createAction<void>(
  'minimumPlayersRequirmentMet',
);
export const minimumPlayersRequirmentUnMet = createAction<void>(
  'minimumPlayersRequirmentMet',
);

export const gameStarted = createAction<void>('gameStarted');
export const newWord = createAction<string>('newWord');
export const shuffleWordList = createAction('shuffleWordList');

export type GameSettings = {
  roomName: string;
  language: string;
  dictionary: {
    name: string;
    json: () => Promise<string[]>;
  };
  roomSize: number;
  nGames: number;
  gameTime: number;
};
export const createGame = createAction<GameSettings>('createGame');
