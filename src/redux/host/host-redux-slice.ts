import { createSlice, createReducer, PayloadAction } from '@reduxjs/toolkit';
import {
  createGame,
  playerJoined,
  shuffleWordList,
  setDrawingPlayer,
  clearCanvas,
} from './sketchActions';
import { LineInfo } from '../../Sketchbook/types';
import { peerOpened } from './peerActions';
import { drawLine } from '../../Sketchbook/drawing';

export const hostSlice = createSlice({
  name: 'hostSlice',
  initialState: {
    lines: [] as LineInfo[],
    word: '',
    drawerId: '',
    gameCreated: false,
    players: [] as {
      name: string;
      playerId: string;
      peerId: string;
    }[],
    minimumPlayers: 2,
    dictionary: 'animals',
    nGames: 20,
    hostId: '',
    gameTime: 1000 * 60 * 2,
    wordList: [],
  },
  reducers: {
    drawLine: (
      state,
      action: PayloadAction<{
        line: LineInfo;
      }>,
    ) => {
      state.lines.push(action.payload.line);
    },
    clearCanvas: state => {
      state.lines = [];
    },
    setWordList: (state, action: PayloadAction<string[]>) => {
      state.wordList = action.payload;
    },
  },
  extraReducers: {
    [drawLine.toString()]: (
      state,
      action: PayloadAction<{
        line: LineInfo;
      }>,
    ) => {
      state.lines.push(action.payload.line);
    },
    [clearCanvas.toString()]: state => {
      state.lines = [];
    },
    [setDrawingPlayer.toString()]: (state, action: PayloadAction<string>) => {
      state.drawerId = action.payload;
    },
    [createGame.toString()]: (state, action: ReturnType<typeof createGame>) => {
      state.gameCreated = true;
      state.dictionary = action.payload.dictionary.name;
      state.nGames = action.payload.nGames;
      state.minimumPlayers = action.payload.roomSize;
    },
    [peerOpened.toString()]: (state, action: ReturnType<typeof peerOpened>) => {
      state.hostId = action.payload;
    },
    [shuffleWordList.toString()]: state => {},
    [playerJoined.toString()]: (
      state,
      action: ReturnType<typeof playerJoined>,
    ) => {
      const existingPlayerIndex = state.players.findIndex(player => {
        return player.playerId === action.payload.playerId;
      });
      if (existingPlayerIndex > -1) {
        state.players[existingPlayerIndex] = {
          playerId: action.payload.playerId,
          name: action.payload.name,
          peerId: action.payload.peerId,
        };
      } else {
        state.players.push({
          playerId: action.payload.playerId,
          name: action.payload.name,
          peerId: action.payload.peerId,
        });
      }
    },
  },
});
