import { shuffle } from 'lodash';

type PlayerId = string;

type FluxAction = {
  type: string;
  payload: any;
};

type GameSettings = {
  roundLength: number;
  numberOfRounds: number;
  broadCast: (action: FluxAction) => void;
  sendToPlayer: (player: PlayerId, action: FluxAction) => void;
  peerIds: PlayerId[];
};

class SketchGame {
  private currentDrawingPlayer: PlayerId;

  private players: PlayerId[] = [];

  private currentWordIndex: number;

  private currentRound: number;

  private playerScores: Map<PlayerId, number>;

  private startTime: number;

  private roundLength: number;

  private endTime: number;

  private wordList: string[] = [];

  private subject: string;

  private broadCast: (action: FluxAction) => void;

  private sendToPlayer: (player: PlayerId, action: FluxAction) => void;

  constructor(gameSettings: GameSettings) {
    this.initialize(gameSettings).then(() => {
      this.startGame();
    });
  }

  private initialize = async (gameSettings: GameSettings): Promise<void> => {
    await this.setWordList();
    this.roundLength = gameSettings.roundLength;
    this.broadCast = gameSettings.broadCast;
    this.sendToPlayer = gameSettings.sendToPlayer;
    gameSettings.peerIds.forEach(this.addPlayer);
    [this.currentDrawingPlayer] = this.players;
  };


  private get currentWord(): string {
    return this.wordList[this.currentWordIndex];
  }

  public checkMessage = (peerId: string, message: string): void => {
    if (message === this.currentWord) {
      this.declareWinner(peerId);
    }
  };

  private declareWinner = (peerId: string): void => {
    this.broadCast({
      type: 'have-winner',
      payload: { peerId, word: this.currentWord },
    });
    this.startNewRound();
  };

  private startNewRound = (): void => {
    this.startTime = Date.now();
    this.endTime = this.startTime + this.roundLength;

    const newDrawingPlayer = this.getNewDrawingPlayer();
    this.findNewWord();
    this.broadCast({
      type: 'new-round-started',
      payload: {
        drawer: newDrawingPlayer,
      },
    });
    this.sendToPlayer(newDrawingPlayer, {
      type: 'word-to-draw',
      payload: this.currentWord,
    });
  };

  public startGame = (): void => {
    this.startNewRound();
  };

  private findNewWord = (): void => {
    const maxIndex = this.wordList.length - 1;
    this.currentWordIndex += 1;
    if (this.currentWordIndex > maxIndex) {
      this.currentWordIndex = 0;
    }
  };

  private setWordList = async (): Promise<void> => {
    const list = await import('../../word-lists/animals.en.json');
    this.subject = 'Animals';
    this.wordList = shuffle(list);
  };

  public addPlayer = (playerId: PlayerId): void => {
    this.players.push(playerId);
    this.broadCast({
      type: 'player-added',
      payload: playerId,
    });
  };

  public removePlayer = (playerId: PlayerId): void => {
    this.players = this.players.filter(player => {
      return player !== playerId;
    });
    this.broadCast({
      type: 'player-removed',
      payload: playerId,
    });
  };
}

export const gameReducer = 0;