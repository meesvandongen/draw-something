import { Middleware } from 'redux';
import { PayloadAction } from '@reduxjs/toolkit';
import PT, { DataConnection } from 'peerjs';
import {
  connectionDataSent,
  connectionEstablished,
  peerOpened,
  peerError,
  connectionClosed,
  connectToPeer,
  sendToPeer,
  remotePeerMessage,
  sendToAllPeers,
} from './host/peerActions';

class MockPeer {
  on() {}
}

const Peer: typeof PT =
  typeof window !== 'undefined' ? require('peerjs').default : MockPeer;

export const RECREATE_WORKER = 'RECREATE_WORKER';

const createPeerMiddleware = (): Middleware => ({ dispatch }) => {
  let ownPeerId = null;
  // const connections = new Map<string, DataConnection>();
  const peer = new Peer(null, {
    debug: 2,
  });

  const connections = new Map<string, DataConnection>();

  peer.on('open', (id: string) => {
    if (peer.id === null) {
      console.log('Received null id from peer open');
      peer.id = ownPeerId;
    } else {
      ownPeerId = peer.id;
    }
    dispatch(peerOpened(id));
  });
  peer.on('disconnected', () => {
    peer.reconnect();
  });
  peer.on('connection', (newConnection: DataConnection) => {
    const peerId = newConnection.peer;
    dispatch(connectionEstablished(peerId));
    newConnection.on('data', data => {
      dispatch(
        connectionDataSent({
          peerId,
          data,
        }),
      );
    });
    newConnection.on('close', () => {
      dispatch(connectionClosed(peerId));
      connections.delete(peerId);
    });
    connections.set(peerId, newConnection);
  });

  peer.on('close', () => {
    console.log('Connection destroyed');
  });
  peer.on('error', err => {
    dispatch(peerError(err));
  });
  /*
      when the worker posts a message back, dispatch the action with its payload
      so that it will go through the entire middleware chain
    */
  return next => (action: PayloadAction<any>) => {
    if (action.type === connectToPeer.toString()) {
      peer.connect(action.payload);
    } else if (action.type === sendToPeer.toString()) {
      const {
        payload: { peerId, action: remoteAction },
      } = action;
      console.log(remoteAction, action, peerId);
      if (peerId === ownPeerId) {
        return next(remoteAction);
      }
      connections.get(peerId).send(remotePeerMessage(remoteAction));
    } else if (action.type === sendToAllPeers.toString()) {
      const {
        payload: remoteAction,
      }: PayloadAction<PayloadAction<any>> = action;
      console.log(remoteAction);
      connections.forEach(remotePeer => {
        remotePeer.send(remoteAction);
      });
      return next(remoteAction);
    }
    // if (action.payload && action.payload.forWorker === true) {
    //   worker.postMessage(action);
    // }
    // always pass the action along to the next middleware
    return next(action);
  };
};

export default createPeerMiddleware;
