import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PlayerSent } from '../host/sketchActions';

type PostMessageAction = PayloadAction<
  {
    content: string;
  } & PlayerSent
>;

export const chatSlice = createSlice({
  name: 'chatSlice',
  initialState: {
    messages: [] as {
      author: string;
      content: string;
    }[],
  },
  reducers: {
    postMessage: (state, action: PostMessageAction): void => {
      state.messages.push({
        author: action.payload.playerId,
        content: action.payload.content,
      });
    },
    postSystemMessage: (
      state,
      action: PayloadAction<{
        content: string;
      }>,
    ): void => {
      state.messages.push({
        author: 'SYSTEM',
        content: action.payload.content,
      });
    },
  },
  extraReducers: {},
});
