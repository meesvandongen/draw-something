import createSagaMiddleware from 'redux-saga';
import { configureStore, combineReducers } from '@reduxjs/toolkit';
// import { persistStore } from 'redux-persist';
import { hostSlice } from './host/host-redux-slice';
import { rootSaga } from './host/saga';
import { chatSlice } from './chat/chat-redux-slice';
import createPeerMiddleware from './peerMiddleware';
import participant from './host/participant-redux-slice';

const rootReducer = combineReducers({
  host: hostSlice.reducer,
  chat: chatSlice.reducer,
  participant: participant.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;

const sagaMiddleware = createSagaMiddleware();
const peerMiddleware = createPeerMiddleware();

const middleware = [sagaMiddleware, peerMiddleware];
const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
});

// export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga);

// if (process.env.NODE_ENV === 'development' && module.hot) {
//   module.hot.accept('./state', () => {
//     const newRootReducer = require('./state').default;
//     store.replaceReducer(newRootReducer);
//   });
// }

export default store;
