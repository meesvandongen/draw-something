import { useSelector } from 'react-redux';

const HostSideBarPart = () => {
  const hostId = useSelector(state => state.host.hostId);

  return (
    <>
      <span>Host ID: {hostId}</span>
    </>
  );
};

export default HostSideBarPart;
