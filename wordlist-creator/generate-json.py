from wordfreq import word_frequency
import json
from pathlib import Path

lang = 'en'


def sortList(word):
    return word_frequency(word, lang)


with open("./wordlist-creator/animals/en.txt") as file:
    words = file.read().splitlines()
    words.sort(key=sortList, reverse=True)
    Path('./wordlist-creator/output/animals/').mkdir(parents=True, exist_ok=True)
    with open("./wordlist-creator/output/animals/en.json", "w+") as jsonFile:
        json.dump(words, jsonFile)
